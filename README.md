# polar
How to use this class? 
- Download the file 'polar.py', put it in the same directory as your scipt. 
- Turn on the Polar OH1 and wear it.
- Use one of the following two methods in your script:

## method 1, in the foreground

    from polar import Polar
    with Polar(address) as polar:
    	print("heart rate {:d}".format(polar.rate))

The heart rate is printed once.
		
## method 2, in the background, register a function (print in this example)

    from polar import Polar
    with Polar(address, print) as polar:
    	time.sleep(5) 
    	
The function 'print' will be called each time the polar yields a heart rate for 5 seconds.
