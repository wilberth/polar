#!/usr/bin/env python3

import time
import asyncio
import threading
import logging

from bleak import BleakClient


address       = "A0:9E:1A:71:96:37"
model_uid     = "00002a24-0000-1000-8000-00805f9b34fb"
battery_uid   = "00002a19-0000-1000-8000-00805f9b34fb"
heartbeat_uid = "00002a37-0000-1000-8000-00805f9b34fb"

class Polar:
	timeout = 10
	def __init__(self, address=address, callback=None):
		self.running = True
		self._rate = -1
		self.userCallback = callback
		threading.Thread(target=asyncio.run, args=(self.run(address),)).start() # mix of paradigm
		
	def __enter__(self):
		t0 = time.time()
		while self.rate==-1 and time.time()-t0 < self.timeout:
			time.sleep(.1)
		return self

	@property
	def rate(self):
		return self._rate

	def callback(self, sender, data):
		self._rate = data[1] # in 1/minute
		logging.debug("live {:d} bpm ".format(self._rate))
		if self.userCallback:
			self.userCallback(self._rate)

	async def run(self, address):
		async with BleakClient(address) as client:
			model = await client.read_gatt_char(model_uid)
			model = model.decode("utf-8")
			battery = await client.read_gatt_char(battery_uid)
			battery = battery[0]
			logging.debug("device: {:s}, battery: {:d}%, press enter to stop".format(model, battery))
			await client.start_notify(heartbeat_uid, self.callback)
			while await client.is_connected() and self.running:
				await asyncio.sleep(.1)

	def __exit__(self, type, value, traceback):
		self.running = False
		
if __name__ == "__main__":
	# How to use this class:
	# method 1, in the foreground
	with Polar(address) as polar:
		print("heart rate {:d}".format(polar.rate))
		
	time.sleep(2)
		
	# method 2, in the background, register a function (print in this example)
	with Polar(address, print) as polar:
		time.sleep(5)
